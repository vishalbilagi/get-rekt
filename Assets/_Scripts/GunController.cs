﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GunController : NetworkBehaviour {


    public Transform[] subMachinePoint = new Transform[2];
    public AudioClip submachine;
    public AudioClip rocket;
    public AudioSource[] source = new AudioSource[2];
    public Transform rocketLauncherPoint;
    public float fireRate = 0.1f;
    public float rocketTimeElapsed;
    public float timer = 0;
    public float rocketCooldown = 2f;
    public ParticleSystem[] fx = new ParticleSystem[2];
    public GameObject Rocket;
    public float launchForce;

    public float explosionForce;
    public float explosionRadius;
    public float upModifier;
    public ParticleSystem explosionEffect;
    public AudioSource shockwaveSound;

    public Transform grappleTransform;
    public float pullForceMultiplier;


    private void Update()
    {
        if (Input.GetButton("RB"))
        {
            if (timer > fireRate)
            {
                source[0].PlayOneShot(submachine);
                StartCoroutine("PlayFlare");
                Debug.DrawRay(subMachinePoint[0].position, subMachinePoint[0].transform.forward * 100, Color.red);
                Debug.DrawRay(subMachinePoint[1].position, subMachinePoint[1].transform.forward * 100, Color.red);
                timer = 0;
            }
            else
            {
                timer += Time.unscaledDeltaTime;
            }
        }

        if (Input.GetButtonDown("Jump"))
        {
            FireHook();
        }

        rocketTimeElapsed = Mathf.Clamp(rocketTimeElapsed += Time.deltaTime, 0, 3);
    }

    void FireHook()
    {
        RaycastHit hitInfo;
        Rigidbody rb;
        if(Physics.Raycast(grappleTransform.position,grappleTransform.forward,out hitInfo))
        {
            Debug.DrawRay(grappleTransform.position, grappleTransform.forward * 1000, Color.green);
            if (hitInfo.collider.attachedRigidbody != null)
            {
                rb = hitInfo.collider.attachedRigidbody;
                rb.AddForce((transform.position - rb.position).normalized * pullForceMultiplier, ForceMode.Impulse);
            }
        }
    }

    void PlayFlare()
    {
        fx[0].Play();
        fx[1].Play();
    }

    void FixedUpdate () {

        if (Input.GetButton("LB"))
        {
            if (rocketTimeElapsed > rocketCooldown)
            {

                source[1].Play();
                rocketTimeElapsed = 0;
                //
                GameObject Bazooka;
                Bazooka = Instantiate(Rocket, rocketLauncherPoint.position, rocketLauncherPoint.rotation) as GameObject;
                Bazooka.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * launchForce, ForceMode.Acceleration);
               
            }
            

            //Rocket.GetComponent<Rigidbody>().velocity = new Vector3(0, 100, 0);
        }

        if (Input.GetButtonDown("Fire3"))
        {
            explosionEffect.Play();
            shockwaveSound.Play();
            Collider[] collider;
            List<Rigidbody> rigidbodies = new List<Rigidbody>();
            collider = Physics.OverlapSphere(transform.position, explosionRadius);
            for (int i = 0; i < collider.Length; i++)
            {
                Debug.Log(collider[i].name);
            }
            foreach (var collided in collider)
            {
                if(collided.name == "TST")
                {
                    continue;
                }
                else if((collided.attachedRigidbody != null && !rigidbodies.Contains(collided.attachedRigidbody)))
                rigidbodies.Add(collided.attachedRigidbody);
            }

            foreach (var rb in rigidbodies)
            {
                rb.AddExplosionForce(explosionForce, transform.position, explosionRadius, upModifier,ForceMode.Impulse);
            }

        }

    }
}
