﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CameraController : NetworkBehaviour {

    public float camTurnSpeed = 5;
    public float camTurnI;
    public float camVert;
    private const float MaxX = 20f;
    private const float MinX = 1;
    public Transform targetPlayer;
    
    public Camera camera;
 
	
	// Update is called once per frame
	void Update () {
        camTurnI = Input.GetAxis("RSH");
        camVert = Input.GetAxis("RSV");
        
        //camera.transform.LookAt(targetPlayer);

        camera.transform.RotateAround(targetPlayer.position, Vector3.up, camTurnI * camTurnSpeed);
        //camera.transform.RotateAround(targetPlayer.position, transform.TransformDirection(Vector3.right), camVert * camTurnSpeed);

        //if (camera.transform.rotation.x < minx)
        //{
        //    camera.transform.rotation = new quaternion(minx, 0, 0, 1);
        //}
        //if(camera.transform.rotation.x > maxx)
        //{
        //    camera.transform.rotation = new quaternion(maxx, 0, 0, 1); ;
        //}
    }
}
