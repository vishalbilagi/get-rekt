﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPoint : MonoBehaviour {

    public MeshRenderer controlpointColor;
    private int blueCount;
    private int redCount;

    private void Start()
    {
        
        blueCount = redCount = 0;
    }

    private void Update()
    {
        if(blueCount>0 && redCount > 0)
        {
            //Pause capture
        }
        else if (blueCount > 0 && redCount == 0)
        {
            //Capture by blue
            
        }
        else if(redCount > 0 && blueCount == 0)
        {
            //Capture by red
        }

        Debug.Log(blueCount);
    }

    private void OnTriggerEnter(Collider player)
    {
        if (player.CompareTag("Player"))
        {
            player.GetComponent<VehicleController>().cp = this;

            if (player.GetComponent<VehicleController>().team == "Blue")
            {
                blueCount++;
            }
            else if (player.GetComponent<VehicleController>().team == "Red")
            {
                redCount++;
            }
        }
    }

    private void OnTriggerExit(Collider player)
    {
        if (player.CompareTag("Player"))
        {
            player.GetComponent<VehicleController>().cp = null;
            if (player.GetComponent<VehicleController>().team == "Blue")
            {
                blueCount--;
            }
            else if (player.GetComponent<VehicleController>().team == "Red")
            {
                redCount--;
            }
        }
    }

    void DeathInCP(string team)
    {
        Debug.Log(team);
        if (team == "Blue")
            blueCount--;
        else if (team == "Red")
            redCount--;
    }
}
