﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bazooka : MonoBehaviour {

    public GameObject Explosion;
    public AudioSource gunSound;
    public AudioClip blast;
    public AudioSource auds;

    private void Start()
    {
        gunSound = GameObject.FindGameObjectWithTag("RocketSound").GetComponent<AudioSource>();
        auds = GetComponent<AudioSource>();

        auds.clip = blast;
    }

    public void OnCollisionEnter(Collision collision)
    {
        gunSound.Stop();
        auds.Play();
        GameObject g = Instantiate(Explosion, collision.contacts[0].point, new Quaternion(0,0,0,0)) as GameObject;
        this.enabled = false;
        Destroy(gameObject,2);
        Destroy(g,2);
    }



}
