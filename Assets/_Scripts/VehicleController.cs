﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class VehicleController : NetworkBehaviour {
    //Vehicle properties
    public float enginePower = 150.0f;
    public float brakeForce = 10f;
    public float boostForce = 50f;
    public float maxSteer = 35f;
    public float maxSpeed = 30f;
    public Transform[] tires = new Transform[6];
    public Transform turret;
    //Boost particle effect and its sound
    
    public GameObject boostEffect;
    public AudioClip boostStart;
    public AudioClip boostLoop;
    public AudioClip boostEnd;
    //Team identifier string
    public string team;
    //Control point reference used for sending message to Control Points upon vehicle's destruction
    public ControlPoint cp;
    
    //Vechicle properties
    private float power;
    private float brake;
    private float steer;
    private Rigidbody vehicle;
    private Transform tVehicle;
    private WheelCollider[] wheels = new WheelCollider[4];
    
    //Audio sources on vehicle
    private AudioSource engine;
    private AudioSource boostSource;
    public GameObject sceneCam;
    public Camera vehicleCam;
    

	void Start () {

        vehicle = GetComponent<Rigidbody>();
        tVehicle = GetComponent<Transform>();

        wheels = GetComponentsInChildren<WheelCollider>();

        engine = GetComponent<AudioSource>();

        boostSource = GetComponents<AudioSource>()[1];
        
        vehicle.centerOfMass = new Vector3(0, 0.5f, 0);

        vehicleCam = GetComponentInChildren<Camera>();
        //if (isLocalPlayer)
        //{
        //    sceneCam = GameObject.FindGameObjectWithTag("SceneCam");
        //    sceneCam.gameObject.SetActive(false);
        //}
    }

    private void FixedUpdate()
    {

        //Change engine sound pitch with respect to vehicle's velocity
        Vector3 velocity;
        velocity = vehicle.velocity;
        float forwardSpeed = transform.InverseTransformDirection(vehicle.velocity).z;
        float revs = Mathf.Abs(forwardSpeed) * 0.1f;
        engine.pitch = Mathf.Clamp(revs, .05f, 1.8f);
        

        if(Input.GetButtonDown("Fire1") && Input.GetAxisRaw("Accelerate") > 0)
        {
            boostSource.PlayOneShot(boostStart);
        }

        if (Input.GetButton("Fire1") && Input.GetAxisRaw("Accelerate") > 0)
        {
            if (!boostSource.isPlaying)
            {
                boostSource.clip = boostLoop;
                boostSource.loop = true;
                boostSource.Play();
            }

            boostEffect.SetActive(true);

            vehicle.AddRelativeForce(new Vector3(0, 0, 1) * boostForce, ForceMode.Impulse);
            
        }

        if(Input.GetButtonUp("Fire1"))
        {
            boostEffect.SetActive(false);
            boostSource.Stop();
            boostSource.clip = null;
            boostSource.loop = false;
            boostSource.PlayOneShot(boostEnd);
        }


        //Limit vehicle's velocity
        vehicle.velocity = Vector3.ClampMagnitude(vehicle.velocity, maxSpeed);
        
    }
    
    void Update () {
        
        power = Input.GetAxis("Accelerate") * enginePower * Time.deltaTime * 250.0f;
        steer = Input.GetAxis("Horizontal") * maxSteer;
        brake = 0;

        //Handle turret rotation
        float angle;
        angle = vehicleCam.transform.localEulerAngles.y;
        turret.localEulerAngles = new Vector3(0, 0, angle + 90);

        //For testing purposes only
        if (Input.GetKeyDown(KeyCode.X))
        {
            Destroy(gameObject);
        }

        //Set front tires steer angle
        tires[0].localEulerAngles = new Vector3(tires[0].transform.localEulerAngles.x, tires[0].localEulerAngles.y, steer);
        tires[1].localEulerAngles = new Vector3(tires[1].transform.localEulerAngles.x, tires[1].localEulerAngles.y, steer);
        
        //Set all tires rotation depending on wheel collider rpm        
        tires[4].Rotate(0, -wheels[0].rpm / 60 * 360 * Time.deltaTime, 0);
        tires[5].Rotate(0, -wheels[1].rpm / 60 * 360 * Time.deltaTime, 0);
        tires[2].Rotate(0, -wheels[2].rpm / 60 * 360 * Time.deltaTime, 0);
        tires[3].Rotate(0, -wheels[3].rpm / 60 * 360 * Time.deltaTime, 0);

        //Apply brakes on all wheels
        if (Input.GetButton("Fire2"))
        {
            brake = brakeForce * vehicle.mass;  
        }

        //Set front wheel colliders' steer angle
        wheels[0].GetComponent<WheelCollider>().steerAngle = steer;
        wheels[1].GetComponent<WheelCollider>().steerAngle = steer;

        if (brake > 0.0)
        {
            wheels[0].GetComponent<WheelCollider>().brakeTorque = brake;
            wheels[1].GetComponent<WheelCollider>().brakeTorque = brake;
            wheels[2].GetComponent<WheelCollider>().brakeTorque = brake;
            wheels[3].GetComponent<WheelCollider>().brakeTorque = brake;
            wheels[0].GetComponent<WheelCollider>().motorTorque = 0.0f;
            wheels[1].GetComponent<WheelCollider>().motorTorque = 0.0f;
            wheels[2].GetComponent<WheelCollider>().motorTorque = 0.0f;
            wheels[3].GetComponent<WheelCollider>().motorTorque = 0.0f;
        }
        else
        {
            wheels[0].GetComponent<WheelCollider>().brakeTorque = 0;
            wheels[1].GetComponent<WheelCollider>().brakeTorque = 0;
            wheels[2].GetComponent<WheelCollider>().brakeTorque = 0;
            wheels[3].GetComponent<WheelCollider>().brakeTorque = 0;
            wheels[0].GetComponent<WheelCollider>().motorTorque = power;
            wheels[1].GetComponent<WheelCollider>().motorTorque = power;
            wheels[2].GetComponent<WheelCollider>().motorTorque = power;
            wheels[3].GetComponent<WheelCollider>().motorTorque = power;
        }

    }

    private void OnDestroy()
    {
        if(cp!=null)
        cp.SendMessage("DeathInCP", team);
    }
}
