﻿using UnityEngine;
using UnityEngine.Networking;


public class PlayerSetup : NetworkBehaviour{

    [SerializeField]
    public Behaviour[] stuffToDisable;

    private void Start()
    {
        if (!isLocalPlayer)
        {
            for (int i = 0; i < stuffToDisable.Length; i++)
            {
                stuffToDisable[i].enabled = false;
            }
        }
    }

}
